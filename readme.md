## Docker commands

Building: `docker build -t example/example_build:0.1 -f DockerfileBuildEnv .`

Running: `docker run -it --name=indi_ascom --mount type=bind,source=${PWD},target=/src --privileged --add-host=host.docker.internal:host-gateway -p 8000:8000 example/example_build:0.1 bash`

CMAKE: `cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug ../`