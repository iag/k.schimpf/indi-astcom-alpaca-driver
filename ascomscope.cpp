#include "ascomscope.h"
#include <cpr/cpr.h>
#include "libindi/indicom.h"
#include <nlohmann/json.hpp>

using json = nlohmann::json;

static std::unique_ptr<AscomScope> ascomScope(new AscomScope());

AscomScope::AscomScope()
{
    DBG_SCOPE = INDI::Logger::getInstance().addDebugLevel("Scope Verbose", "SCOPE");
}


///////////////////////////////////////////////////////////////////////////////////////
/// Establish connection to the telescope
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::Connect()
{
    LOG_INFO("Connecting...");
    cpr::Response r = cpr::Put(cpr::Url{host + api_endpoint + "connected"},
                               cpr::Payload{{"Connected", "true"}});

    if (r.status_code != 200)
    {
        return false;
    }

    SetTimer(getCurrentPollingPeriod());

    IDMessage(getDeviceName(), "Alpaca device connected successfully!");
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// Disconnect from the telescope
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::Disconnect()
{
    LOG_INFO("Disconnecting...");
    cpr::Response r = cpr::Put(cpr::Url{host + api_endpoint + "connected"},
                               cpr::Payload{{"Connected", "false"}});

    if (r.status_code != 200)
        return false;


    IDMessage(getDeviceName(), "Alpaca device disconnected successfully!");
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// This function is called to initialize the driver properties for the first time.
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::initProperties()
{
    addDebugControl();

    LOG_INFO("Init...");
    INDI::Telescope::initProperties();

    SetTelescopeCapability(TELESCOPE_CAN_GOTO | TELESCOPE_CAN_ABORT | TELESCOPE_CAN_SYNC, 0);

    initGuiderProperties(getDeviceName(), GUIDE_TAB);
    addAuxControls();
    setDriverInterface(getDriverInterface() | GUIDER_INTERFACE);

    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// updateProperties is called whenever Connect/Disconnect event is triggered.
/// If we are now connected to the camera, we might want to query some parameters.
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::updateProperties()
{
    INDI::Telescope::updateProperties();

    if (isConnected())
    {
        defineProperty(&GuideNSNP);
        defineProperty(&GuideWENP);
    }
    else
    {
        deleteProperty(GuideNSNP.name);
        deleteProperty(GuideWENP.name);
    }

    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// Sets properties
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n)
{
     if (!strcmp(name, GuideNSNP.name) || !strcmp(name, GuideWENP.name))
     {
         processGuiderProperties(name, values, names, n);
         return true;
     }
  
     return INDI::DefaultDevice::ISNewNumber(dev, name, values, names, n);
}


///////////////////////////////////////////////////////////////////////////////////////
/// Handshakes with the alpaca server
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::Handshake()
{
    LOG_INFO("Handshaking...");
    cpr::Response r = cpr::Get(cpr::Url{host + "/ping"});

    if (r.status_code != 200)
        return false;

    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// Gets default device name
///////////////////////////////////////////////////////////////////////////////////////
const char *AscomScope::getDefaultName()
{
    return "Ascom-Alpaca Telescope";
}


///////////////////////////////////////////////////////////////////////////////////////
/// Goto ra dec coordinates
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::Goto(double ra, double dec)
{
    LOG_INFO("Syncing...");
    LOGF_INFO("RA: %lf | DEC: %lf", ra, dec);

    cpr::AsyncResponse fr = cpr::PutAsync(cpr::Url{host + api_endpoint + "slewtocoordinates"},
                               cpr::Payload{{"RightAscension", std::to_string(ra)},
                                            {"Declination", std::to_string(dec)}});
    fr.wait();

    cpr::Response r = fr.get();

    if (r.status_code != 200)
    {
        LOG_INFO("FAILED");
        return false;
    }

    TrackState = SCOPE_SLEWING;

    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// TODO: Aborts Motion
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::Abort()
{
    TrackState = SCOPE_IDLE;
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// Reads telescope position and motion status
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::ReadScopeStatus()
{
    LOG_INFO("Reading status...");

    bool pos_state = getPosition();
    bool mo_state = getMotionStatus();
    //bool gu_state = getGuideStatus();

    LOGF_INFO("Motion Status: %.0f", TrackState);

    return pos_state && mo_state;// && gu_state;
}


///////////////////////////////////////////////////////////////////////////////////////
/// Reads telescope position
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::getPosition() {
    cpr::Response r;
    json data;

    r = cpr::Get(cpr::Url{host + api_endpoint + "declination"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);
    double dec = data["Value"];

    r = cpr::Get(cpr::Url{host + api_endpoint + "rightascension"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);
    double ra = data["Value"];

    NewRaDec(ra, dec);

    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// Reads telescope motion status
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::getMotionStatus() {
    cpr::Response r;
    json data;

    r = cpr::Get(cpr::Url{host + api_endpoint + "slewing"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);

    if(data["Value"])
    {
        TrackState = SCOPE_SLEWING;
        return true;
    }

    r = cpr::Get(cpr::Url{host + api_endpoint + "tracking"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);

    if(data["Value"])
    {
        TrackState = SCOPE_TRACKING;
    }

    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// Pulse guides the telescope north
///////////////////////////////////////////////////////////////////////////////////////
IPState AscomScope::GuideNorth(uint32_t ms)
{
    LOGF_DEBUG("Guiding: N %.0f ms", ms);

    if(!Guide(AscGuideNorth, ms))
    {
        return IPS_ALERT;
    }

    return IPS_OK;
    
}


///////////////////////////////////////////////////////////////////////////////////////
/// Pulse guides the telescope south
///////////////////////////////////////////////////////////////////////////////////////
IPState AscomScope::GuideSouth(uint32_t ms)
{
    LOGF_DEBUG("Guiding: S %.0f ms", ms);

    if(!Guide(AscGuideSouth, ms))
    {
        return IPS_ALERT;
    }

    return IPS_OK;
    
}


///////////////////////////////////////////////////////////////////////////////////////
/// Pulse guides the telescope east
///////////////////////////////////////////////////////////////////////////////////////
IPState AscomScope::GuideEast(uint32_t ms)
{
    LOGF_DEBUG("Guiding: E %.0f ms", ms);

    if(!Guide(AscGuideEast, ms))
    {
        return IPS_ALERT;
    }

    return IPS_OK;
    
}


///////////////////////////////////////////////////////////////////////////////////////
/// Pulse guides the telescope west
///////////////////////////////////////////////////////////////////////////////////////
IPState AscomScope::GuideWest(uint32_t ms)
{
    LOGF_DEBUG("Guiding: W %.0f ms", ms);

    if(!Guide(AscGuideWest, ms))
    {
        return IPS_ALERT;
    }

    return IPS_OK;
    
}


///////////////////////////////////////////////////////////////////////////////////////
/// Processes pulse guiding requests
///////////////////////////////////////////////////////////////////////////////////////
bool AscomScope::Guide(ASCOM_DIRECTION direction, uint32_t ms)
{
    int dir = direction;
    cpr::AsyncResponse fr = cpr::PutAsync(cpr::Url{host + api_endpoint + "pulseguide"},
                               cpr::Payload{{"Direction", std::to_string(dir)},
                                            {"Duration", std::to_string(ms)}});
    fr.wait();

    cpr::Response r = fr.get();

    if (r.status_code != 200)
    {
        LOG_ERROR("Guiding FAILED");
        return false;
    }
    return true;
}

bool AscomScope::getGuideStatus() {
    cpr::Response r;
    json data;

    r = cpr::Get(cpr::Url{host + api_endpoint + "ispulseguiding"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);

    if(!data["Value"])
    {
        GuideNSNP.s = IPS_IDLE;
        GuideWENP.s = IPS_IDLE;
        return true;
    }

    return true;
}
