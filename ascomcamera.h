#pragma once

#include <indiccd.h>
#include "indielapsedtimer.h"

class AscomCamera : public INDI::CCD {
public:
    AscomCamera() = default;

    // General device functions
    bool Connect() override;
    bool Disconnect() override;

    const char *getDefaultName() override;

    bool initProperties() override;
    bool updateProperties() override;

    // CCD specific functions
    bool StartExposure(float duration);

protected:
    void TimerHit();
    virtual bool UpdateCCDFrame(int x, int y, int w, int h);
    virtual bool UpdateCCDBin(int binx, int biny);
    virtual bool UpdateCCDFrameType(INDI::CCDChip::CCD_FRAME fType);

private:
    int downloadImage();
    bool setupParams();

    INDI::ElapsedTimer m_ElapsedTimer;
    double ExposureRequest;

    const std::string host = "http://pyobs-alpaca:8080";
    const std::string api_endpoint = "/api/v1/camera/0/";
};