
#pragma once

#include "libindi/inditelescope.h"
#include "libindi/indiguiderinterface.h"

class AscomScope : public INDI::Telescope, public INDI::GuiderInterface  {
public:
    AscomScope();

protected:
    bool Connect() override;
    bool Disconnect() override;

    bool Handshake() override;

    const char *getDefaultName() override;
    bool initProperties() override;
    bool updateProperties() override;

    virtual bool ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n) override ;

    // Telescope specific functions
    bool ReadScopeStatus() override;
    bool Goto(double, double) override;
    bool Abort() override;

    virtual IPState GuideNorth(uint32_t ms) override;
    virtual IPState GuideSouth(uint32_t ms) override;
    virtual IPState GuideEast(uint32_t ms) override;
    virtual IPState GuideWest(uint32_t ms) override;

private:
    bool getPosition();
    bool getMotionStatus();
    bool getGuideStatus();
    
    typedef enum {AscGuideNorth, AscGuideSouth, AscGuideEast, AscGuideWest} ASCOM_DIRECTION;
    bool Guide(ASCOM_DIRECTION direction, uint32_t ms);

    uint8_t DBG_SCOPE { INDI::Logger::DBG_IGNORE };
    const std::string host = "http://pyobs-alpaca:8080";
    const std::string api_endpoint = "/api/v1/telescope/0/";
};
