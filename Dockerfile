FROM ubuntu:20.04

LABEL Description="INDI production environment"

SHELL ["/bin/bash", "-c"]

RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN apt -y --no-install-recommends install build-essential devscripts debhelper fakeroot cdbs software-properties-common cmake
RUN add-apt-repository ppa:mutlaqja/ppa
RUN apt -y --no-install-recommends install libindi-dev libnova-dev libz-dev libgsl-dev
RUN apt -y --no-install-recommends install git libssl-dev libcfitsio-dev

WORKDIR /usr/app/src/

COPY ascomcamera.cpp ascomcamera.h ascomscope.cpp ascomscope.h CMakeLists.txt config.h.cmake indi_astcom.xml.cmake ./
ADD cmake_modules ./cmake_modules

RUN mkdir -p /usr/app/src/build/

WORKDIR /usr/app/src/build

RUN cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug ../
RUN make

CMD ["indiserver", "-p", "8000", "./ascomcamera" ,"./ascomscope"]