#include "ascomcamera.h"

#include <cpr/cpr.h>
#include "libindi/indicom.h"
#include <nlohmann/json.hpp>
#include <memory>

#include <iostream>
using namespace std;

using json = nlohmann::json;
std::unique_ptr<AscomCamera> ascomCamera(new AscomCamera());

///////////////////////////////////////////////////////////////////////////////////////
/// Establish connection to the camera
///////////////////////////////////////////////////////////////////////////////////////
bool AscomCamera::Connect()
{
    LOG_INFO("Connecting...");
    cpr::Response r = cpr::Put(cpr::Url{host + api_endpoint + "connected"},
                               cpr::Payload{{"Connected", "true"}});

    if (r.status_code != 200)
    {
        return false;
    }

    SetTimer(getCurrentPollingPeriod());

    IDMessage(getDeviceName(), "Alpaca device connected successfully!");
    return true;
}

///////////////////////////////////////////////////////////////////////////////////////
/// Disconnect from the camera
///////////////////////////////////////////////////////////////////////////////////////
bool AscomCamera::Disconnect()
{
    LOG_INFO("Disconnecting...");
    cpr::Response r = cpr::Put(cpr::Url{host + api_endpoint + "connected"},
                               cpr::Payload{{"Connected", "false"}});

    if (r.status_code != 200)
        return false;


    IDMessage(getDeviceName(), "Alpaca device disconnected successfully!");
    return true;
}

const char *AscomCamera::getDefaultName()
{
    return "Ascom-Alpaca CCD";
}

///////////////////////////////////////////////////////////////////////////////////////
/// This function is called to initialize the driver properties for the first time.
///////////////////////////////////////////////////////////////////////////////////////
bool AscomCamera::initProperties()
{
    // Always call parent initProperties first.
    INDI::CCD::initProperties();

    // Next, let's setup which capabilities are offered by our camera?
    uint32_t cap = CCD_CAN_BIN | CCD_CAN_SUBFRAME | CCD_HAS_COOLER | CCD_HAS_SHUTTER;
    SetCCDCapability(cap);

    // Add configuration for Debug
    addDebugControl();

    // We're done!
    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// updateProperties is called whenever Connect/Disconnect event is triggered.
/// If we are now connected to the camera, we might want to query some parameters.
///////////////////////////////////////////////////////////////////////////////////////
bool AscomCamera::updateProperties()
{
    // Call parent update properties first
    INDI::CCD::updateProperties();

    if (isConnected())
    {
        // Let's get parameters now from CCD
        setupParams();

        // Start the timer
        SetTimer(getCurrentPollingPeriod());
    }

    return true;
}

bool AscomCamera::setupParams()
{
    cpr::Response r;
    json data;


    float pixel_size;
    int bit_depth;
    int x_1, y_1, x_2, y_2;

    ///////////////////////////
    // 1. Get Pixel size
    ///////////////////////////

    r = cpr::Get(cpr::Url{host + api_endpoint + "pixelsizex"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);
    pixel_size = data["Value"];

    ///////////////////////////
    // 2. Get Frame
    ///////////////////////////

    x_1 = y_1 = 0;

    // Get y size
    r = cpr::Get(cpr::Url{host + api_endpoint + "camaraxsize"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);
    x_2 = data["Value"];

    // Get x size
    r = cpr::Get(cpr::Url{host + api_endpoint + "camaraysize"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);
    y_2 = data["Value"];

    ///////////////////////////
    // 3. Get temperature
    ///////////////////////////
    r = cpr::Get(cpr::Url{host + api_endpoint + "ccdtemperature"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);
    TemperatureN[0].value = data["Value"];

    LOGF_INFO("The CCD Temperature is %f", TemperatureN[0].value);
    IDSetNumber(&TemperatureNP, nullptr);

    ///////////////////////////
    // 4.Get bit depth
    ///////////////////////////
    r = cpr::Get(cpr::Url{host + api_endpoint + "bitdepth"});
    if (r.status_code != 200)
        return false;

    data = json::parse(r.text);
    bit_depth = data["Value"];

    ///////////////////////////
    // 5.Set buffer
    ///////////////////////////
    SetCCDParams(x_2 - x_1, y_2 - y_1, bit_depth, pixel_size, pixel_size);

    // Now we usually do the following in the hardware
    // Set Frame to LIGHT or NORMAL
    // Set Binning to 1x1
    /* Default frame type is NORMAL */

    // Let's calculate required buffer
    int nbuf;
    nbuf = PrimaryCCD.getXRes() * PrimaryCCD.getYRes() * PrimaryCCD.getBPP() / 8; //  this is pixel cameraCount
    nbuf += 512;                                                                  //  leave a little extra at the end
    PrimaryCCD.setFrameBufferSize(nbuf);

    return true;
}


///////////////////////////////////////////////////////////////////////////////////////
/// Start Exposure
///////////////////////////////////////////////////////////////////////////////////////
bool AscomCamera::StartExposure(float duration)
{
    json data;
    INDI::CCDChip::CCD_FRAME imageFrameType = PrimaryCCD.getFrameType();

    bool light = (imageFrameType == INDI::CCDChip::LIGHT_FRAME);

    cpr::AsyncResponse fr = cpr::PutAsync(cpr::Url{host + api_endpoint + "startexposure"},
                               cpr::Payload{{"Duration", std::to_string(duration)},
                                            {"Light", std::to_string(light)}}); // TODO: Change dynamically
    fr.wait();

    cpr::Response r = fr.get();

    if (r.status_code != 200)
    {
        LOG_ERROR("FAILED Exposure");
        return false;
    }

    PrimaryCCD.setExposureDuration(duration);
    ExposureRequest = duration;

    m_ElapsedTimer.start();
    LOGF_INFO("Taking a %g seconds frame...", ExposureRequest);
    InExposure = true;

    return true;
}

///////////////////////////////////////////////////////////////////////////////////////
/// Update Camera Frame Type
///////////////////////////////////////////////////////////////////////////////////////
bool AscomCamera::UpdateCCDFrameType(INDI::CCDChip::CCD_FRAME fType)
{
    INDI::CCDChip::CCD_FRAME imageFrameType = PrimaryCCD.getFrameType();

    if (fType == imageFrameType)
        return true;

    switch (imageFrameType)
    {
        case INDI::CCDChip::BIAS_FRAME:
            LOG_ERROR("Bias frames are not available!");
            break;
        case INDI::CCDChip::DARK_FRAME:
            break;
        case INDI::CCDChip::LIGHT_FRAME:
            break;
        case INDI::CCDChip::FLAT_FRAME:
            LOG_ERROR("Flat frames are not available!");
            return false;
            break;
    }

    PrimaryCCD.setFrameType(fType);

    return true;
}

///////////////////////////////////////////////////////////////////////////////////////
/// Update Camera Region of Interest ROI
///////////////////////////////////////////////////////////////////////////////////////
bool AscomCamera::UpdateCCDFrame(int x, int y, int w, int h)
{
    /* Add the X and Y offsets */
    long x_1 = x;
    long y_1 = y;

    long bin_width  = x_1 + (w / PrimaryCCD.getBinX());
    long bin_height = y_1 + (h / PrimaryCCD.getBinY());

    if (bin_width > PrimaryCCD.getXRes() / PrimaryCCD.getBinX())
    {
        LOGF_INFO("Error: invalid width requested %d", w);
        return false;
    }
    else if (bin_height > PrimaryCCD.getYRes() / PrimaryCCD.getBinY())
    {
        LOGF_INFO("Error: invalid height request %d", h);
        return false;
    }

    /**********************************************************
    *
    *
    *
    *  IMPORRANT: Put here your CCD Frame dimension call
    *  The values calculated above are BINNED width and height
    *  which is what most CCD APIs require, but in case your
    *  CCD API implementation is different, don't forget to change
    *  the above calculations.
    *  If there is an error, report it back to client
    *  e.g.
    *  LOG_INFO( "Error, unable to set frame to ...");
    *  return false;
    *
    *
    **********************************************************/

    // Set UNBINNED coords
    PrimaryCCD.setFrame(x_1, y_1, w, h);

    int nbuf;
    nbuf = (bin_width * bin_height * PrimaryCCD.getBPP() / 8); //  this is pixel count
    nbuf += 512;                                               //  leave a little extra at the end
    PrimaryCCD.setFrameBufferSize(nbuf);

    LOGF_DEBUG("Setting frame buffer size to %d bytes.", nbuf);

    return true;
}

///////////////////////////////////////////////////////////////////////////////////////
/// Update Camera Binning
///////////////////////////////////////////////////////////////////////////////////////
bool AscomCamera::UpdateCCDBin(int binx, int biny)
{
    cpr::Response r;
    json data;

    if (binx != biny)
    {
        LOG_ERROR("Unable to set asymmetric bins!");
        return false;
    }

    r = cpr::Put(cpr::Url{host + api_endpoint + "binx"}, cpr::Payload{{"BinX", std::to_string(binx)}});
    if (r.status_code != 200)
        return false;

    PrimaryCCD.setBin(binx, biny);

    return UpdateCCDFrame(PrimaryCCD.getSubX(), PrimaryCCD.getSubY(), PrimaryCCD.getSubW(), PrimaryCCD.getSubH());
}

///////////////////////////////////////////////////////////////////////////////////////
/// Download the image from the camera. Create random image.
///////////////////////////////////////////////////////////////////////////////////////
int AscomCamera::downloadImage()
{
    cpr::Response r;
    json data;
    uint16_t buffer;
    uint8_t *image = PrimaryCCD.getFrameBuffer();
    int width      = PrimaryCCD.getSubW() / PrimaryCCD.getBinX();
    int height     = PrimaryCCD.getSubH() / PrimaryCCD.getBinY();
    int bytePerPixel = PrimaryCCD.getBPP() / 8;

    LOG_INFO("Downloading...");

    r = cpr::Get(cpr::Url{host + api_endpoint + "imagearrayvariant"});
    if (r.status_code != 200)
        return false;

    LOG_INFO("Parsing...");

    data = json::parse(r.text);
    json value = data["Value"];

    LOG_INFO("Arraybuilding...");

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {

            try {
                buffer = value[i][j];

                uint8_t partA = static_cast<uint8_t>((buffer & 0xFF));
                uint8_t partB = static_cast<uint8_t>(buffer >> 8);

                image[i * width * bytePerPixel + j * bytePerPixel] = partA;
                image[i * width * bytePerPixel + j * bytePerPixel+1] = partB;

            } catch (const nlohmann::json_abi_v3_11_2::detail::type_error& e)
            {
                LOGF_INFO("Invalid value found while parsing: %d", data["Value"][i][j]);
                return false;
            }
        }
            
    }
        

    LOG_INFO("Download complete.");

    ExposureComplete(&PrimaryCCD);

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////
/// TimerHit is the main loop of the driver where it gets called every 1 second
/// by default. Here you perform checks on any ongoing operations and perhaps query some
/// status updates like temperature.
///////////////////////////////////////////////////////////////////////////////////////
void AscomCamera::TimerHit()
{
    if (isConnected() == false)
        return;

    // Are we in exposure? Let's check if we're done!
    if (InExposure)
    {
        // Seconds elapsed
        double timeLeft = ExposureRequest - m_ElapsedTimer.elapsed() / 1000.0;
        if (timeLeft <= 0)
        {
            /* We're done exposing */
            LOG_INFO("Exposure done, downloading image...");

            PrimaryCCD.setExposureLeft(0);
            InExposure = false;
            // Download Image
            downloadImage();
        }
        else
            PrimaryCCD.setExposureLeft(timeLeft);
    }

    

    SetTimer(getCurrentPollingPeriod());
    return;
}