<?xml version="1.0" encoding="UTF-8"?>
<driversList>
   <devGroup group="Indi Ascom">
      <device label="Indi Ascom Alpaca Telescope" manufacturer="IAG">
         <driver name="Indi Ascom Alpaca Telescope">ascomscope</driver>
         <version>@CDRIVER_VERSION_MAJOR@.@CDRIVER_VERSION_MINOR@</version>
      </device>
      <device label="Indi Ascom Alpaca Camera" manufacturer="IAG">
         <driver name="Indi Ascom Alpaca Camera">ascomcamera</driver>
         <version>@CDRIVER_VERSION_MAJOR@.@CDRIVER_VERSION_MINOR@</version>
      </device>
   </devGroup>
</driversList>
